package com.company;

public class Main {

    public static void main(String[] args) {
            House school = new House();
            House kidgarden = new House();

            school.set(4, 2011, "Школа");
            kidgarden.set(2, 2019, "Детский сад");

            school.printAll();
            kidgarden.printAll();

            System.out.println (school.countYears (2020));
            System.out.println (kidgarden.countYears (2020));
        }

    }

